# Médiathèque

Ce dépôt a été créé afin de s'entraîner en UML. 
A partir du cahier des charges suivant, réalisez les diagrammes UML demandés par le formateur. 

Pour vous aider : 
 - [uml-diagrams.org](https://www.uml-diagrams.org/)

## Règles générales
La médiathèque contient un certain nombre de documents disponibles à la consultation ou à l’emprunt ; les personnes désirant emprunter des ouvrages pour une durée et à un tarif donnés doivent s’inscrire en tant que client.
- Les clients s’inscrivent auprès d’un employé de la médiathèque, et empruntent et rendent un document par l’intermédiaire d’un employé de la médiathèque.
- L’inscription des clients consiste à remplir une fiche datée sur laquelle sont notées les informations suivantes : nom et prénom du client ainsi que son adresse.
- Les catégories de client actuelles permettent au client de choisir de payer à chaque emprunt effectué (catégorie « à tarif normal » ou « à tarif réduit ») ou de régler une cotisation annuelle (catégorie « abonné »).
- Outre le tarif, la catégorie de client conditionne les critères d’emprunt suivants :
le nombre de documents empruntables et la durée de prêt (voir ci-dessous).

Dans le cas « tarif réduit », un justificatif est demandé à l’inscription, puis à chaque date anniversaire. Les justificatifs à prévoir sont les suivants : carte étudiant/scolaire, carte vermeil et carte « à caractère social ».

Les documents disponibles sont des CD audios, des DVD ou des livres. Chaque document est repéré par un code unique et une localisation (salle/rayon) dans la médiathèque. Certains documents ne peuvent pas être empruntés, mais uniquement consultés sur place. Les informations communes aux documents sont les suivantes : le titre, l’auteur (écrivain, groupe ou metteur en scène) et l’année de sortie. Le système devra permettre de
disposer de statistiques d’utilisation, telles que le nombre d’emprunts effectués pour les différentes catégories et genres de documents. 

Les autres informations des documents sont les suivantes :
- les CD possèdent un genre musical (par exemple, « classique », « variétés françaises », « variétés internationales », « compilation ») et une classification (par exemple, « opéra » pour le genre « classique », « hardcore » pour le genre « variétés internationales » ou « rock » pour le genre « compilation ») ;
- Les DVD possèdent un genre de vidéo (« documentaire », « comédie »...), une durée d’émission et une mention légale de diffusion (restrictions d’usage) ; cette mention doit être disponible lors de l’emprunt du DVD pour permettre un éventuel contrôle ;
- Les livres possèdent un genre (« roman », « policier », etc.) et un nombre de pages.

Les genres précisés sont libres ; ils sont donnés aux clients à titre indicatif pour aider au choix lors d’un emprunt.
- Chaque sortie de document entraîne la constitution d’une fiche d’emprunt. Sur cette fiche, sont notés le client emprunteur, la date de début du prêt, le document emprunté et la date limite de restitution. Les durées de prêt dépendent du type de document et de la catégorie du client (voir les règles ci-dessous) ;
- La médiathèque met à la disposition des clients des ordinateurs pour qu’ils consultent le catalogue, leurs emprunts, et puissent mettre à jour leur adresse.

Le système de gestion doit prévoir toute opération d’ajout et de suppression de clients et de documents. Les informations les concernant ne sont pas construites par le système (par exemple, la localisation des documents dans les locaux), mais supposées fournies lors de l’invocation de ces opérations. Le système doit permettre de réviser les catégories et leurs conditions associées. D’autre part, les formats de la plupart des informations sont libres (chaînes de caractères) ; le système doit toutefois veiller à la cohérence des informations stockées (impossibilité d’avoir deux clients ou deux documents avec le même nom, emprunter deux fois le même document, etc.).

## Règles de prêt
L’emprunt d’un document par un client obéit à certaines règles :
- un client ne peut pas emprunter plus d’un certain nombre de documents fixé par sa catégorie : 2 pour la catégorie « à tarif réduit », 5 pour la catégorie « à tarif normal » et 10 pour la catégorie « abonné ». Dès que ce nombre maximal est atteint pour un client donné, tout nouveau prêt doit être impossible ;
- tout client qui n’a pas restitué un document avant sa date limite de restitution ne peut plus faire de nouvel emprunt tant qu’il n’a pas régularisé sa situation, ceci même si le nombre maximal d’emprunts n’est pas atteint. Pour ce faire, à chaque demande d’emprunt, le système vérifie s’il est à jour dans ses restitutions ; si ce n’est pas le cas, l’emprunt n’est pas autorisé.
- l’ensemble des fiches d’emprunt est parcouru chaque jour afin de repérer s’il existe des documents pour lesquels la date limite de restitution est dépassée. Pour chacune de ces fiches trouvées, la date de rappel de la fiche est mise à la date du jour, le client concerné est marqué et la médiathèque envoie une lettre de rappel que nous considérons hors système. Les fiches ayant fait l’objet d’un rappel font l’objet d’une relance hebdomadaire.
- le tarif des emprunts dépend du document et du client. Le tarif du document est fixé par son type : 0,5 € pour un livre, 1 € pour un CD audio et 1,5 € pour un DVD. La règle pour les clients « à tarif normal » est de payer le montant fixé pour chaque document emprunté (indiqué auparavant). Le tarif appliqué aux clients « à tarif réduit » est la moitié du « tarif normal ». Les « abonnés » réglant une cotisation annuelle empruntent les documents gratuitement ;
- la durée des emprunts dépend du document et du client. Chaque type de document est empruntable pour une durée dite nominale : 2 semaines pour un DVD, 4 semaines pour un CD audio et 6 semaines pour un livre. Ensuite, la durée de prêt est modifiée selon la catégorie de client : aucun changement — la durée nominale — pour un client à tarif normal, la moitié de cette durée pour un client à tarif réduit et le double de cette durée pour un abonné ;
- un client peut changer de statut. Par exemple, un client abonné devient un
client à tarif normal si son abonnement n’est pas renouvelé à la date
anniversaire. De la même façon, un client à tarif réduit devient un client à tarif
normal si aucun justificatif n’est fourni à la date anniversaire.